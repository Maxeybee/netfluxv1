import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { ListFilmsComponent } from './list-films/list-films.component';
import { ListFilmsByCategoryComponent } from './list-films-by-category/list-films-by-category.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    ListFilmsComponent,
    ListFilmsByCategoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
