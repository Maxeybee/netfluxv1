import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListFilmsByCategoryComponent } from './list-films-by-category.component';

describe('ListFilmsByCategoryComponent', () => {
  let component: ListFilmsByCategoryComponent;
  let fixture: ComponentFixture<ListFilmsByCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListFilmsByCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListFilmsByCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
