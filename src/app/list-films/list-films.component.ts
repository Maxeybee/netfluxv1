import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Page } from '../shared/models/page';
import { Observable } from 'rxjs';

@Component({
  selector: "app-list-films",
  templateUrl: "./list-films.component.html",
  styleUrls: ["./list-films.component.css"]
})
export class ListFilmsComponent implements OnInit {
  pages: Page;
  data;

  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.getFilms();
  }

  getFilms() {
    this.http
      .get(
        "https://api.themoviedb.org/3/movie/top_rated?sort_by=popularity.desc&language=fr-FR&api_key=87dfa1c669eea853da609d4968d294be"
      )
      .subscribe((data: Page)=> {
        this.pages = data;
        console.log(data);
      });
  }
}
