import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListFilmsComponent } from './list-films/list-films.component';
import { ListFilmsByCategoryComponent } from './list-films-by-category/list-films-by-category.component';


const routes: Routes = [
  {path:'listfilms', component:ListFilmsComponent},
  {path:'listcategory', component:ListFilmsByCategoryComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
