import { Film } from './film';

export class Page {
    
    page: number;
    total_results: number;
    total_pages: number;
    results: Film[];
}
